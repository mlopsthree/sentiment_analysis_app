from pydantic import BaseModel


class TextRequest(BaseModel):
    """
    Sentiment analyser

    Attributes:
        text (str): text
    """

    text: str

    class Config:
        """Model example."""

        schema_extra = {
            "example": {
                "text": "This is a very good program",
            }
        }
