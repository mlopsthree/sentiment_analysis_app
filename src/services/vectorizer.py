import pickle
from pathlib import Path
from numpy import ndarray
import pandas as pd


class BowVectorizer:
    '''
    Takes a plain text and vectorize it with a pretrained vectorizer
    '''
    def __init__(self, model_path: Path) -> None:
        self.vectorizer = pickle.load(model_path.open("rb"))

    def vectorize(self, text: str) -> ndarray:
        text = pd.Series(text)
        return self.vectorizer.transform(text)
