import pickle
from pathlib import Path
from loguru import logger
from src.services.vectorizer import BowVectorizer

import warnings
from sklearn.exceptions import InconsistentVersionWarning
warnings.filterwarnings("ignore", category=InconsistentVersionWarning)


class SentimentAnalyser:
    '''
    This classifier analyses given vectorized text and predict
    if it is good or bad sentiment.
    '''
    vectorizer = BowVectorizer(Path('models/bow_vectorizer.pkl'))
    predictor = pickle.load(Path('models/log_reg.pkl').open("rb"))

    @classmethod
    def predict_sentiment(cls, text: str) -> str:
        vectorized_text = cls.vectorizer.vectorize(text)
        score = cls.predictor.predict(vectorized_text)[0]

        if score == 1:
            sentiment = 'BAD'
        elif score == 2:
            sentiment = 'GOOD'
        logger.info(sentiment)
        return sentiment
