FROM python:3.11-slim

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN pip install --no-cache-dir poetry=="1.8.3"

RUN poetry install --no-dev

COPY . /app