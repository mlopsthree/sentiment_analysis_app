
from src.services.predictor import SentimentAnalyser
from loguru import logger
from src.connections.broker import celery_app


@celery_app.task(name="predict")
def predict_sentiment(text) -> str | None:
    """
    Predict text sentiment

    """
    try:
        prediction = SentimentAnalyser.predict_sentiment(text)
        return prediction

    except Exception as ex:
        logger.error([ex])
        return None
