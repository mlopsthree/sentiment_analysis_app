from fastapi import APIRouter, BackgroundTasks
from src.models.requests import TextRequest
from src.connections.broker import celery_app
from src.services.utils import print_logger_info

router = APIRouter()


@router.post("/predict/")
async def predict_sentiment(text_request: TextRequest,
                            background_tasks: BackgroundTasks) -> dict:
    """
    Predict the sentiment of the given text using a machine learning model.

    This endpoint sends a task to a Celery worker to perform the sentiment prediction.
    The result of the prediction is then logged asynchronously using the provided
    BackgroundTasks object.

    Args:
        text_request (TextRequest): The request object containing the text to analyze.
        background_tasks (BackgroundTasks): The BackgroundTasks object for adding tasks.

    Returns:
        dict: The result of the sentiment prediction.
    """

    async_result = celery_app.send_task("predict", args=[text_request.text])

    result = async_result.get()
    background_tasks.add_task(
        print_logger_info,
        text_request.text,
        result,
    )

    return result
