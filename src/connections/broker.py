from celery import Celery

celery_app = Celery('tasks',
                    backend='redis://redis:6379',
                    broker='pyamqp://guest:guest@rabbitmq:5672')
